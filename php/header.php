<!doctype html>  
<html lang="en">  
<head>  
  <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />  
  <meta name="author" content="Jake Rocheleau">  
  <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">  
  <meta name="HandheldFriendly" content="true">  
  <meta name="viewport" content="width=device-width, initial-scale=1,maximum-scale=1,user-scalable=no">  
  <link rel="stylesheet" type="text/css" href="http://fonts.googleapis.com/css?family=Simonetta:400,900|Balthazar">  
  
  <link rel="stylesheet" type="text/css" href="css/normalize.css">
  <link rel="stylesheet" type="text/css" href="css/custom.css">
  <link rel="stylesheet" type="text/css" href="css/responsive.css">   
   
  <!--[if lt IE 9]>  
    <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>  
    <script src="http://css3-mediaqueries-js.googlecode.com/svn/trunk/css3-mediaqueries.js"></script>  
  <![endif]-->  
  
    <title>Advantage</title>
    
</head>
<body>
    
    <style>
        
    </style>

    <div class="container">
        <div class="c-center">
            <div class="clearfix-top">&nbsp;</div>
            <div class="clearfix-top">&nbsp;</div>
            <div class="clearfix-top">&nbsp;</div>
            <div class="clearfix-top">&nbsp;</div>
            <div class="clearfix-top">&nbsp;</div>
            
            <div>
                <span class="fbig mt5 left">Dan Chiuzbãian</span>
                <span class="left rmt10 rml10">
                    <a href="index.php?lang=ro"><img src="img/ro.png" class="lang-icon" alt="" /></a>
                    <a href="index.php?lang=en" class="mr_r10"><img src="img/en.png" class="lang-icon" alt="" /></a>
                </span>
            </div>
            <div class="clearfix">&nbsp;</div>
            <div class="clearfix">&nbsp;</div>
            <div class="clearfix">&nbsp;</div>
            
            <div>
                <div class="left mrb10 rw260">
                    <img src="img/bg_1.jpg" alt="" />
                </div>
                <div class="left mrb10 rw_i">
                    <img src="img/bg_2.jpg" alt="" />
                </div>
                <div class="left mrb10 rw_i">
                    <img src="img/bg_3.jpg" alt="" />
                </div>
                <div class="left rw_i">
                    <img src="img/bg_4.jpg" alt="" />
                </div>
            </div>
            <div class="clear"></div>            
            <div class="left big-pic">
                <div class="left rw260">
                    <img src="img/img_1.jpg" alt="" />
                </div>
            </div>
            <div class="left">
                <div class="left rw130 mrb10">
                    <img src="img/img_3.jpg" alt="" />
                </div>
                <div class="left rw130 mrb10">
                    <img src="img/img_2.jpg" alt="" />
                </div>
                <div class="left rw130 rmr5 rw_i">
                    <img src="img/img_4.jpg" alt="" />
                </div>
            </div>
            <div class="left">
                <div class="left rw130 mrb10 rw_i">
                    <img src="img/img_5.jpg" alt="" />
                </div>      
                <div class="left rw130 mrb10 rw_i">
                    <img src="img/img_6.jpg" alt="" />
                </div>
                <div class="left rw130 rmt5 rw_i">
                    <img src="img/img_7.jpg" alt="" />
                </div>
            </div>
            <div class="clear">&nbsp;</div>
            <div>