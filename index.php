<?php include 'php/header.php' ?>

    <?php
        
        /**
        * format yyyy-mm-dd
        *  Calculate age
        * @param mixed $birthdate
        */
        function getAge($birthdate) {
            $c = date('Y');
            $y = date('Y',strtotime($birthdate));
            return $c-$y;
        }
        
        /**
        * Set language, default 'en'
        * @var mixed
        */
        $lang = (isset($_GET['lang'])) ? $_GET['lang'] : 'en';
        
    ?>

    <?php if ($lang == 'en') : ?>
        <p class="fm">
            I am the co-founder and the majority share-holder of the  <a href="http://www.qsoundsoft.com/en_index.htm" target="_blank">QSound Soft</a> company. 
            We implement and manage automation projects for the main <a href="http://radio.qsoundsoft.ro/en/" target="_blank">radio</a> networks and stations in Romania, offering 
            them specialized software and hardware products. 
            We also provide full services to companies that use ambient music or InStore Radio application 
        </p>
        <p class="fm grey">
            Date of birth: November 29th, 1976 (<?= getAge('1976-11-29') ?> years old)<br />
            Marital status: married to Maria Chiuzbăian<br />
            Parents: Gavril Chiuzbăian, Aurelia Chiuzbăian<br />
        </p>
        <p class="fm grey">
            <strong>Education:</strong><br />
            2000 - 2002: Post-graduate studies in <a href="http://www.utcluj.ro" target="_blank">Management</a>, Technical University of Cluj-Napoca<br />
            1995 - 2000: <span class="rw_i">Bachelor level studies, </span>Faculty of Automation and Computer Science,<br /> 
                                      <a href="http://www.utcluj.ro" target="_blank" class="rml80">Technical University of Cluj-Napoca</a><br />
            1992 - 1995: Secondary studies, the <a href="http://www.sincaibm.ro" target="_blank">Gheoghe Șincai College</a> in Baia-Mare<br />
        </p>
        <p class="fm grey">
            <strong>Professional Background:</strong><br /> 
            • 2002 - 2010 - Co-founder and Sales Manager, <a href="http://www.ikonsoft.ro" target="_blank">Ikon SOFT</a><br />
            • 2002 - 2005 - Co-founder of Mediasoft, later known as <a href="http://www.napocasoftware.ro" target="_blank">Napoca Software</a><br />
            • 2005 - Business project development and IT solutions for the radio station <a href="http://www.infotrafic.biz" target="_blank">InfoTrafic - The radio station on your street!</a>
        </p>
        <p class="fm grey">
            <strong>Contact:</strong><br />
            Adress: Aurel Vlaicu Street, N° 4/109 DIV, Cluj-Napoca, Romania<br />
            Mobile phone number: + 40 744 814 294<br />
            E-mail: danq@qsoft.ro<br /> 
        </p>
        <div class="left mt10">
            <div class="left mrb10 rw50">
                <a href="http://www.qsoundsoft.com/en_index.htm" target="_blank"><img src="img/link_1.png" alt="" /></a>
            </div>
            <div class="left mrb10 rw50">
                <a href="http://www.infotrafic.biz" target="_blank"><img src="img/link_2.png" class="mt10" alt="" /></a>
            </div>
            <div class="left mrb10 rw50">
                <a href="http://radio.qsoundsoft.ro/en/" target="_blank"><img src="img/link_3.png" alt="" /></a>
            </div>
            <div class="left mrb10 rw50_x">
                <a href="http://instoreradio.qsoundsoft.ro/?lang=en" target="_blank"><img src="img/link_4.png" alt="" /></a>
            </div>
        </div>
        
        
    <?php elseif ($lang == 'ro') : ?>
        <p class="fm">
            Sunt co-fondator şi actionar majoriar al firmei <a href="http://www.qsoundsoft.com" target="_blank">QSound SOFT</a> gestionând şi implementând proiecte de automatizare 
            radio pentru principalele reţele şi staţii independente de <a href="http://www.qsoundsoft.com/ro_parteneri.htm" target="_blank">radio</a> din Romania. 
            Compania pe care o conduc este specializată în realizarea de produse software sau hardware dedicate staţiilor de 
            radio şi furnizeaza servicii complete pentru afacerile ce utilizează muzica ambientală sau InStore Radio. </p>
        <p class="fm grey">
            Data naşterii: 29 noiembrie 1976 (<?= getAge('1976-11-29') ?> ani)<br />
            Soţie: Maria Chiuzbăian<br />
            Parinţi: Gavril Chiuzbăian, Aurelia Chiuzbăian<br />
        </p>
        <p class="fm grey">
            <strong>Studii:</strong><br /> 
            2000 - 2002: Şcoala de studii postuniversitare-specializarea <a href="http://www.utcluj.ro" target="_blank">Management UTCN</a> <br />
            1995 - 2000: <a href="http://www.utcluj.ro" target="_blank">Universitatea Tehnică Cluj-Napoca</a>, secţia Calculatoare<br />
            1992 - 1995: <a href="http://www.sincaibm.ro" target="_blank">Colegiul Gheorge Şincai</a> Baia Mare<br />
        </p>
        <p class="fm grey">
            <strong>Alte Afaceri:</strong> <br />
            • 2002 - 2010 - Cofondator şi director de vânzări al firmei <a href="http://www.ikonsoft.ro" target="_blank">Ikon SOFT</a> <br />
            • 2002 - 2005 - Cofondator al firmei Mediasoft - devenită <a href="http://www.napocasoftware.ro" target="_blank">Napoca Software</a> <br />
            • 2005 - Dezvoltare proiect de business şi soluţie informatică pentru <a href="http://www.infotrafic.biz" target="_blank">InfoTrafic - Radioul de pe strada ta!</a>  <br />
        </p>
        <p class="fm grey">
            <strong>Contact</strong><br />
            Adresa: Cluj-Napoca, România - Str. Aurel Vlaicu nr. 4/109 DIV<br />
            Mobil: +4 0744 814 294<br />
            E-mail: danq@qsoft.ro<br />
        </p>
        <div class="left mt10">
            <div class="left mrb10 rw50">
                <a href="http://www.qsoundsoft.com" target="_blank"><img src="img/link_1.png" alt="" /></a>
            </div>
            <div class="left mrb10 rw50">
                <a href="http://www.infotrafic.biz" target="_blank"><img src="img/link_2.png" class="mt10" alt="" /></a>
            </div>
            <div class="left mrb10 rw50">
                <a href="http://radio.qsoundsoft.com" target="_blank"><img src="img/link_3.png" alt="" /></a>
            </div>
            <div class="left mrb10 rw50_x">
                <a href="http://instoreradio.qsoundsoft.com" target="_blank"><img src="img/link_4.png" alt="" /></a>
            </div>
        </div>
    <?php endif ?>

<?php include 'php/footer.php' ?>